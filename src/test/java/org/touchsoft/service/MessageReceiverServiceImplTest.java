package org.touchsoft.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.touchsoft.exception.MessageTransferReceiveException;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.service.impl.MessageReceiverServiceImpl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;

class MessageReceiverServiceImplTest {

    @Test
    void unsupportedMessageTypeTest() throws MessageTransferReceiveException {
        MessageTransferService messageTransferService = Mockito.mock(MessageTransferService.class);
        Action unsupportedMessage = new Action(ActionType.REGISTER, null);
        Mockito.when(messageTransferService.receive(any(SocketWrapper.class))).thenReturn(unsupportedMessage);

        MessageReceiverServiceImpl messageReceiverService = new MessageReceiverServiceImpl(messageTransferService);
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);
        assertTrue(messageReceiverService.receive(socketWrapper), "Expected 'true' value return");
    }

    @Test
    void supportedMessageTypeTest1() throws MessageTransferReceiveException {
        MessageTransferService messageTransferService = Mockito.mock(MessageTransferService.class);
        Action unsupportedMessage = new Action(ActionType.SEND_MESSAGE, new MessagePayload("test message"));
        Mockito.when(messageTransferService.receive(any(SocketWrapper.class))).thenReturn(unsupportedMessage);

        MessageReceiverServiceImpl messageReceiverService = new MessageReceiverServiceImpl(messageTransferService);
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);
        assertTrue(messageReceiverService.receive(socketWrapper), "Expected 'true' value return");
    }

    @Test
    void supportedMessageTypeTest2() throws MessageTransferReceiveException {
        MessageTransferService messageTransferService = Mockito.mock(MessageTransferService.class);
        Action unsupportedMessage = new Action(ActionType.FORGET_INTERLOCUTOR, new MessagePayload("test message"));
        Mockito.when(messageTransferService.receive(any(SocketWrapper.class))).thenReturn(unsupportedMessage);

        MessageReceiverServiceImpl messageReceiverService = new MessageReceiverServiceImpl(messageTransferService);
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);
        assertTrue(messageReceiverService.receive(socketWrapper), "Expected 'true' value return");
    }


    @Test
    void supportedMessageTypeTest3() throws MessageTransferReceiveException {
        MessageTransferService messageTransferService = Mockito.mock(MessageTransferService.class);
        Action unsupportedMessage = new Action(ActionType.FORGOTTEN, new MessagePayload("test message"));
        Mockito.when(messageTransferService.receive(any(SocketWrapper.class))).thenReturn(unsupportedMessage);

        MessageReceiverServiceImpl messageReceiverService = new MessageReceiverServiceImpl(messageTransferService);
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);
        assertFalse(messageReceiverService.receive(socketWrapper), "Expected 'true' value return");
    }

    @Test
    void exceptionWhileReceivingTest() throws MessageTransferReceiveException {
        MessageTransferService messageTransferService = Mockito.mock(MessageTransferService.class);
        Mockito.when(messageTransferService.receive(any(SocketWrapper.class))).thenThrow(new MessageTransferReceiveException("test ex message"));

        MessageReceiverServiceImpl messageReceiverService = new MessageReceiverServiceImpl(messageTransferService);
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);
        assertFalse(messageReceiverService.receive(socketWrapper), "Expected 'true' value return");
    }
}
