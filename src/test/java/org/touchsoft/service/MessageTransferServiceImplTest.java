package org.touchsoft.service;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.touchsoft.exception.MessageTransferReceiveException;
import org.touchsoft.exception.MessageTransferSendException;
import org.touchsoft.exception.SocketGetInputStreamException;
import org.touchsoft.exception.SocketGetOutputStreamException;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.service.impl.MessageTransferServiceImpl;

import java.io.*;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MessageTransferServiceImplTest {

    private ByteArrayOutputStream response;
    private static MessageTransferServiceImpl messageTransferService;


    @BeforeAll
    static void setUp() {
        messageTransferService = new MessageTransferServiceImpl();
    }

    @BeforeEach
    void prepare() {
        response = new ByteArrayOutputStream();
    }

    @AfterEach
    void clean() throws IOException {
        response.close();
    }

    @Test
    void checkMessageSendExceptionThrowing_1() throws SocketGetOutputStreamException {
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);
        Action action = Mockito.mock(Action.class);
        Mockito.when(socketWrapper.getOutputStream()).thenThrow(new SocketGetOutputStreamException("test ex message"));

        Assertions.assertThrows(MessageTransferSendException.class, () -> {
            messageTransferService.send(socketWrapper, action);
        });
    }

    @Test
    void checkMessageSendExceptionThrowing_2() throws IOException {
        Socket socket = Mockito.mock(Socket.class);
        Mockito.when(socket.getOutputStream()).thenThrow(new IOException("test ex message"));
        SocketWrapper socketWrapper = new SocketWrapper(socket);
        Action action = Mockito.mock(Action.class);

        Assertions.assertThrows(MessageTransferSendException.class, () -> {
            messageTransferService.send(socketWrapper, action);
        });
    }

    @Test
    void checkMessageReceiveExceptionThrowing_1() throws SocketGetInputStreamException {
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);
        Mockito.when(socketWrapper.getInputStream()).thenThrow(new SocketGetInputStreamException("test ex message"));

        Assertions.assertThrows(MessageTransferReceiveException.class, () -> {
            messageTransferService.receive(socketWrapper);
        });
    }

    @Test
    void checkMessageReceiveExceptionThrowing_2() throws IOException {
        Socket socket = Mockito.mock(Socket.class);
        Mockito.when(socket.getInputStream()).thenThrow(new IOException("test ex message"));
        SocketWrapper socketWrapper = new SocketWrapper(socket);

        Assertions.assertThrows(MessageTransferReceiveException.class, () -> {
            messageTransferService.receive(socketWrapper);
        });
    }

    @Test
    void checkMessageSendSuccess() throws IOException, ClassNotFoundException {
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);

        Action actionToSend = new Action(ActionType.SEND_MESSAGE, new MessagePayload("sometext"));

        Mockito.when(socketWrapper.getOutputStream()).thenReturn(response);

        messageTransferService.send(socketWrapper, actionToSend);

        byte[] bytes = response.toByteArray();

        ObjectInputStream objectInput = new ObjectInputStream(new ByteArrayInputStream(bytes));
        Action actionToReceive = (Action) objectInput.readObject();

        assertEquals(actionToSend, actionToReceive, "Request desnot equal to response");
    }

    @Test
    void checkMessageReceiveSuccess() throws IOException {
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);

        Action actionToSend = new Action(ActionType.SEND_MESSAGE, new MessagePayload("sometext"));
        ObjectOutputStream objectOutput = new ObjectOutputStream(response);
        objectOutput.writeObject(actionToSend);
        byte[] bytes = response.toByteArray();

        Mockito.when(socketWrapper.getInputStream()).thenReturn(new ByteArrayInputStream(bytes));

        Action actionToReceive = messageTransferService.receive(socketWrapper);

        assertEquals(actionToSend, actionToReceive, "Request desnot equal to response");
    }
}
