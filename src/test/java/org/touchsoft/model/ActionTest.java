package org.touchsoft.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mockito;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.model.action.payload.impl.Role;
import org.touchsoft.model.action.payload.impl.VisitorData;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

class ActionTest {
    private static Action registerAction;
    private static Action sendMessageAction;

    private static final VisitorData registeredVisitor = new VisitorData("client name", Role.ROLE_CLIENT);
    private static final MessagePayload testMessage = new MessagePayload("test message text");

    @BeforeAll
    static void setUp() {
        registerAction = new Action(ActionType.REGISTER, registeredVisitor);
        sendMessageAction = new Action(ActionType.SEND_MESSAGE, testMessage);
    }

    @ParameterizedTest
    @EnumSource(value = ActionType.class)
    void checkActionType(ActionType actionType) throws NoSuchFieldException, IllegalAccessException {
        Action action = new Action(actionType, null);
        final Field field = action.getClass().getDeclaredField("type");
        field.setAccessible(true);

        assertSame(field.get(action), action.getType(), "value of field 'type' did not match with expected");
    }

    @Test
    void checkPayload() throws NoSuchFieldException, IllegalAccessException {
        final Field field = sendMessageAction.getClass().getDeclaredField("payload");
        field.setAccessible(true);

        assertSame(field.get(sendMessageAction), sendMessageAction.getPayload(), "value of field 'payload' did not match with expected");
    }

    @Test
    void checkMessagePayload() {
        assertSame(testMessage, sendMessageAction.getPayload(), "value of field 'payload' did not match with expected");
    }


    @Test
    void checkMessagePayloadFailed() {
        assertNotEquals(testMessage, registerAction.getPayload(), "value of field 'payload' did not match with expected");
    }


    @ParameterizedTest
    @EnumSource(value = ActionType.class)
    void tesSetter(ActionType actionType) {
        Action action = Mockito.mock(Action.class);
        Mockito.doCallRealMethod().when(action).setType(actionType);
        Mockito.doCallRealMethod().when(action).getType();
        action.setType(actionType);

        assertSame(actionType, action.getType(), "value of field 'type' did not match with expected");
    }
}
