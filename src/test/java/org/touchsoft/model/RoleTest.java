package org.touchsoft.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.touchsoft.model.action.payload.impl.Role;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class RoleTest {
    private final int ROLE_NUMBER = 2;

    @ParameterizedTest
    @MethodSource("parametersProvider")
    void testWithMultiArgMethodSource(Role role, String name) {
        assertSame(role.toString(), name, "Message of role enum element is not as expected");
    }

    private static Stream<Arguments> parametersProvider() {
        return Stream.of(Arguments.of(Role.ROLE_CLIENT, "client"),
                Arguments.of(Role.ROLE_AGENT, "agent"));
    }

    @Test
    void checkRolesCount() {
        assertEquals(Role.values().length, ROLE_NUMBER, "Number of roles in app is not as expected");
    }

    @ParameterizedTest
    @MethodSource("parametersProvider2")
    void testFromString(Role role, String name) {
        assertSame(Role.fromString(name), role, "Message of role enum element is not as expected");
    }

    private static Stream<Arguments> parametersProvider2() {
        return Stream.of(Arguments.of(Role.ROLE_CLIENT, "client"),
                Arguments.of(null, "blabla"),
                Arguments.of(Role.ROLE_AGENT, "agent"));
    }

}
