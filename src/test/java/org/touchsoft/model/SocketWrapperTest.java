package org.touchsoft.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.touchsoft.exception.SocketCreateException;
import org.touchsoft.exception.SocketGetInputStreamException;
import org.touchsoft.exception.SocketGetOutputStreamException;
import org.touchsoft.exception.SocketCloseException;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.assertSame;

public class SocketWrapperTest {

    @Test
    void visitorSocketGetInputStreamFaied() {
        Assertions.assertThrows(SocketGetInputStreamException.class, () -> {
            SocketWrapper socketWrapper = new SocketWrapper(new Socket() {
                @Override
                public InputStream getInputStream() throws IOException {
                    throw new IOException("exc message");
                }
            });
            socketWrapper.getInputStream();
        });
    }

    @Test
    void visitorSocketGetOutputStreamFaied() {
        Assertions.assertThrows(SocketGetOutputStreamException.class, () -> {
            Socket socket = Mockito.mock(Socket.class);
            Mockito.when(socket.getOutputStream()).thenThrow(new IOException("test exception message"));

            SocketWrapper socketWrapper = new SocketWrapper(socket);

            socketWrapper.getOutputStream();
        });
    }

    @Test
    void visitorSocketClosingSocketFailed() {
        Assertions.assertThrows(SocketCloseException.class, () -> {
            Socket socket = Mockito.mock(Socket.class);
            Mockito.doThrow(new IOException("test exception message")).when(socket).close();

            SocketWrapper socketWrapper = new SocketWrapper(socket);

            socketWrapper.closeSocket();
        });
    }

    @Test
    void visitorSocketGetInetAddress() throws UnknownHostException, SocketCreateException {
        InetAddress inetAddress = InetAddress.getByName("127.0.0.1");

        Socket socket = Mockito.mock(Socket.class);
        Mockito.when(socket.getInetAddress()).thenReturn(inetAddress);

        SocketWrapper socketWrapper = new SocketWrapper(socket);

        assertSame(socketWrapper.getInetAddress(), inetAddress, "getInetAddress of VisitorSocket.class returns unexpected data");
    }

    @Test
    void visitorSocketGetLocalAddress() throws UnknownHostException, SocketCreateException {
        InetAddress inetAddress = InetAddress.getByName("127.0.0.1");

        Socket socket = Mockito.mock(Socket.class);
        Mockito.when(socket.getLocalAddress()).thenReturn(inetAddress);

        SocketWrapper socketWrapper = new SocketWrapper(socket);
        assertSame(socketWrapper.getLocalAddress(), inetAddress, "getLocalAddress of VisitorSocket.class returns unexpected data");
    }

    @Test
    void visitorSocketCreationFailed() {
        Assertions.assertThrows(SocketCreateException.class, () -> {
            new SocketWrapper("abc", 1);
        });
    }
}
