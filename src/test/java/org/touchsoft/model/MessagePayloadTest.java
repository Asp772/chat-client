package org.touchsoft.model;

import org.junit.jupiter.api.Test;
import org.touchsoft.model.action.payload.impl.MessagePayload;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MessagePayloadTest {
    @Test
    void testConstructor() throws IllegalAccessException, NoSuchFieldException {
        String message_text = "new message";

        MessagePayload messagePayload = new MessagePayload(message_text);

        final Field field = messagePayload.getClass().getDeclaredField("message");
        field.setAccessible(true);

        assertEquals(field.get(messagePayload), message_text, " value of field 'message' is not equal to passed to constr");
    }

    @Test
    void tesSetter() throws IllegalAccessException, NoSuchFieldException {
        String message_text = "new message";
        String new_text = "another message";

        MessagePayload messagePayload = new MessagePayload(message_text);
        messagePayload.setMessage(new_text);

        final Field field = messagePayload.getClass().getDeclaredField("message");
        field.setAccessible(true);

        assertEquals(field.get(messagePayload), new_text, " value of field 'message' is not equal to passed value in setter method ");

    }

    @Test
    void tesGetter() {
        String message_text = "new message";
        MessagePayload messagePayload = new MessagePayload(message_text);

        assertEquals(messagePayload.getMessage(), message_text, " value of field 'message' is not equal to getter method return");
    }
}
