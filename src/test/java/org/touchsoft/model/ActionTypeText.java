package org.touchsoft.model;

import org.junit.jupiter.api.Test;
import org.touchsoft.model.action.ActionType;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ActionTypeText {
    private final int ACTIONS_NUMBER = 5;

    @Test
    void checkActionsCount() {
        assertEquals(ActionType.values().length, ACTIONS_NUMBER, "Number of actions in app is not as expected");
    }
}
