package org.touchsoft.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.touchsoft.exception.AccepterInitiationException;

class NotifierAccepterTest {
    private final int TEST_PORT = 17;

    @Test
    void wrongDataPassedToConstructor() {

        Assertions.assertThrows(AccepterInitiationException.class, () -> {
            new NotifierAccepter(TEST_PORT);
            new NotifierAccepter(TEST_PORT);
        });
    }
}
