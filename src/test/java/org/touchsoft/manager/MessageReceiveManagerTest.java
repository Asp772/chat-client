package org.touchsoft.manager;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.service.MessageReceiverService;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

public class MessageReceiveManagerTest {
    @ParameterizedTest
    @ValueSource(strings = {"messageReceiverService", "clientSocketReceiver"})
    void checkFieldIsNotNullAfterCreatingTest(String name) throws NoSuchFieldException, IllegalAccessException {
        MessageReceiveManager messageReceiveManager = new MessageReceiveManager(Mockito.mock(MessageReceiverService.class), Mockito.mock(SocketWrapper.class));

        final Field field = messageReceiveManager.getClass().getDeclaredField(name);
        field.setAccessible(true);

        assertNotNull(field.get(messageReceiveManager), " value of field '" + name + "' is null after creating");
    }

    @ParameterizedTest
    @ValueSource(strings = {"threadIsAlive"})
    void checkFlagsAfterCreatingTest(String name) throws NoSuchFieldException, IllegalAccessException {
        MessageReceiveManager messageReceiveManager = new MessageReceiveManager(Mockito.mock(MessageReceiverService.class), Mockito.mock(SocketWrapper.class));

        final Field field = messageReceiveManager.getClass().getDeclaredField(name);
        field.setAccessible(true);

        assertTrue((boolean) field.get(messageReceiveManager), " value of field '" + name + "' is null after creating");
    }

    @Test
    void stopReceivingTest() {
        MessageReceiverService messageReceiverService = Mockito.mock(MessageReceiverService.class);
        SocketWrapper socketWrapper = Mockito.mock(SocketWrapper.class);

        Mockito.when(messageReceiverService.receive(socketWrapper)).thenReturn(false);

        MessageReceiveManager messageReceiveManager = new MessageReceiveManager(messageReceiverService, socketWrapper);
        messageReceiveManager.start();
        messageReceiveManager.stopReceiving();
        assertFalse(messageReceiveManager.isAlive(), "Unexpected continuance of executing");
    }
}
