package org.touchsoft.util;

public class SafePrint {
    public static void safePrintln(String s) {
        synchronized (System.out) {
            System.out.println(s);
        }
    }
}
