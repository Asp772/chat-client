package org.touchsoft.exception;

import java.io.IOException;

public class ClientRunningException extends IOException {
    public ClientRunningException(String message) {
        super(message);
    }
}
