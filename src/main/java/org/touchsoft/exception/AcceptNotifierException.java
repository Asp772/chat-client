package org.touchsoft.exception;

import java.io.IOException;

public class AcceptNotifierException extends IOException {
    public AcceptNotifierException(String message) {
        super(message);
    }
}
