package org.touchsoft.exception;

import java.io.IOException;

public class ClientActionUnhandledException extends IOException {
    public ClientActionUnhandledException(String message) {
        super(message);
    }
}
