package org.touchsoft.exception;

import java.io.IOException;

public class ClientLaunchException extends IOException {
    public ClientLaunchException(String message) {
        super(message);
    }
}
