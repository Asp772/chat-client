package org.touchsoft.exception;

import java.io.IOException;

public class SocketCreateException extends IOException {
    public SocketCreateException(String message) {
        super(message);
    }
}
