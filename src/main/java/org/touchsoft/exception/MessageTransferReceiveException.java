package org.touchsoft.exception;

import java.io.IOException;

public class MessageTransferReceiveException extends IOException {
    public MessageTransferReceiveException(String message){super(message);}
}
