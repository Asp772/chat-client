package org.touchsoft.exception;

import java.io.IOException;

public class ReceiverClosingException extends IOException {
    public ReceiverClosingException(String message) {
        super(message);
    }
}
