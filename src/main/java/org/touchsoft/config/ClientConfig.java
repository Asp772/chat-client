package org.touchsoft.config;

import java.util.ResourceBundle;

public class ClientConfig {
    private static final String PROPERTIES_FILE="application";
    private static final String PORT_NAME="SERVER-PORT";
    private static final String HOST_NAME="SERVER-HOST";

    public static int SERVER_PORT;
    public static String SERVER_HOST;

    static{
        ResourceBundle resourceBundle= ResourceBundle.getBundle(PROPERTIES_FILE);
        SERVER_PORT=Integer.parseInt(resourceBundle.getString(PORT_NAME));
        SERVER_HOST=resourceBundle.getString(HOST_NAME);
    }
}