package org.touchsoft.service;

import org.touchsoft.exception.MessageTransferReceiveException;
import org.touchsoft.exception.MessageTransferSendException;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.SocketWrapper;

public interface MessageTransferService {
    boolean send(SocketWrapper socketWrapper, Action action) throws MessageTransferSendException;

    Action receive(SocketWrapper socketWrapper) throws MessageTransferReceiveException;
}
