package org.touchsoft.service.impl;

import org.touchsoft.exception.*;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.model.action.payload.impl.Role;
import org.touchsoft.model.action.payload.impl.VisitorData;
import org.touchsoft.service.MessageSenderService;
import org.touchsoft.service.MessageTransferService;
import org.touchsoft.util.SafePrint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MessageSenderServiceImpl implements MessageSenderService {

    private MessageTransferService messageTransferService;

    public MessageSenderServiceImpl(MessageTransferService messageTransferService) {
        this.messageTransferService = messageTransferService;
    }

    @Override
    public boolean communicate(SocketWrapper clientSocket) throws ClientRunningException {
        boolean result;
        try {
            String enteredString = getClientCommand();
            String retval[] = enteredString.split(" ", 3);

            switch (retval[0]) {

                case "/leave": {
                    Action clientAction = new Action(ActionType.LEAVE, new MessagePayload("User has left"));
                    messageTransferService.send(clientSocket, clientAction);
                    result = false;

                }
                break;

                default: {
                    Action clientAction = new Action(ActionType.SEND_MESSAGE, new MessagePayload(enteredString));
                    messageTransferService.send(clientSocket, clientAction);
                    result = true;
                }
                break;
            }
        } catch (ClientActionUnhandledException e) {
            System.err.println(e.getMessage());
            result = true;
        } catch (MessageTransferSendException e) {
            throw new ClientRunningException(e.getMessage());
        }
        return result;
    }

    @Override
    public boolean register(SocketWrapper clientSocket) throws ClientLaunchException {
        boolean isRegistered = false;

        boolean continueActivity = true;
        while (continueActivity) {
            try {
                String enteredString = getClientCommand();
                String retval[] = enteredString.split(" ", 3);

                switch (retval[0]) {

                    case "/register": {
                        Role role = Role.fromString(retval[1]);
                        if (role == null) throw new ClientActionUnhandledException("unknown command was entered");
                        VisitorData visitorData = new VisitorData(retval[2], role);

                        ActionType actionType = ActionType.REGISTER;
                        Action clientAction = new Action(actionType, visitorData);
                        this.messageTransferService.send(clientSocket, clientAction);

                        isRegistered = true;
                        continueActivity = false;
                    }
                    break;

                    case "/leave": {
                        continueActivity = false;
                        isRegistered = false;

                        try {
                            clientSocket.closeSocket();
                        } catch (SocketCloseException e) {
                            throw new ClientLaunchException(e.getMessage());
                        }
                    }
                    break;

                    default:
                        break;
                }
            } catch (ClientActionUnhandledException e) {
                System.err.println(e.getMessage());
            } catch (MessageTransferSendException e) {
                throw new ClientLaunchException(e.getMessage());
            }
        }
        return isRegistered;
    }


    private String getClientCommand() throws ClientActionUnhandledException {
        SafePrint.safePrintln("Enter command:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            return br.readLine();
        } catch (IOException e) {
            throw new ClientActionUnhandledException("Wrong text entered");
        }
    }
}
