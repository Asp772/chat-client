package org.touchsoft.service.impl;

import org.apache.log4j.Logger;
import org.touchsoft.exception.MessageTransferReceiveException;
import org.touchsoft.exception.SocketCloseException;
import org.touchsoft.manager.MessageReceiveManager;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.service.MessageReceiverService;
import org.touchsoft.service.MessageTransferService;
import org.touchsoft.util.SafePrint;

public class MessageReceiverServiceImpl implements MessageReceiverService {
    private final static Logger logger = Logger.getLogger(MessageReceiveManager.class.getName());

    private MessageTransferService messageTransferService;

    public MessageReceiverServiceImpl(MessageTransferService messageTransferService) {
        this.messageTransferService = messageTransferService;
    }

    @Override
    public boolean receive(SocketWrapper clientSocketReceiver) {
        boolean flag = true;

        try {
            Action visitorAction = this.messageTransferService.receive(clientSocketReceiver);

            switch (visitorAction.getType()) {
                case SEND_MESSAGE: {
                    MessagePayload messagePayload = (MessagePayload) visitorAction.getPayload();
                    SafePrint.safePrintln("Message received:" + messagePayload.getMessage());
                }
                break;

                case FORGET_INTERLOCUTOR: {
                    MessagePayload messagePayload = (MessagePayload) visitorAction.getPayload();
                    SafePrint.safePrintln(messagePayload.getMessage());
                }
                break;

                case FORGOTTEN: {
                    SafePrint.safePrintln("Goodbye :)");
                    flag = false;
                }
                break;

                default: {
                    logger.warn("unexpected message type" + visitorAction.getType());
                }
                break;
            }

        } catch (MessageTransferReceiveException e) {
            flag = false;
        }
        return flag;
    }


    @Override
    public void stop(SocketWrapper clientSocketReceiver) {
        try {
            if (clientSocketReceiver != null)
                clientSocketReceiver.closeSocket();
        } catch (SocketCloseException e) {
            logger.warn(e.getMessage());
        }
    }

}
