package org.touchsoft.service.impl;

import org.touchsoft.exception.MessageTransferReceiveException;
import org.touchsoft.exception.MessageTransferSendException;
import org.touchsoft.exception.SocketGetInputStreamException;
import org.touchsoft.exception.SocketGetOutputStreamException;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.model.action.Action;
import org.touchsoft.service.MessageTransferService;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MessageTransferServiceImpl implements MessageTransferService {
    @Override
    public boolean send(SocketWrapper socketWrapper, Action action) throws MessageTransferSendException {
        try {
            ObjectOutputStream objectOutput = new ObjectOutputStream(socketWrapper.getOutputStream());
            objectOutput.writeObject(action);

            return true;
        } catch (SocketGetOutputStreamException e) {
            throw new MessageTransferSendException(e.getMessage());
        } catch (IOException e) {
            throw new MessageTransferSendException(e.getMessage());
        }
    }

    @Override
    public Action receive(SocketWrapper socketWrapper) throws MessageTransferReceiveException {
        try {
            ObjectInputStream objectInput = new ObjectInputStream(socketWrapper.getInputStream());
            Object object = objectInput.readObject();

            return (Action) object;
        } catch (SocketGetInputStreamException e) {
            throw new MessageTransferReceiveException(e.getMessage());
        } catch (IOException e) {
            throw new MessageTransferReceiveException(e.getMessage());
        } catch (ClassNotFoundException e) {
            throw new MessageTransferReceiveException(e.getMessage());
        }
    }
}
