package org.touchsoft.service;

import org.touchsoft.exception.ClientLaunchException;
import org.touchsoft.exception.ClientRunningException;
import org.touchsoft.model.SocketWrapper;

public interface MessageSenderService {
    boolean communicate(SocketWrapper clientSocket) throws ClientRunningException;

    boolean register(SocketWrapper clientSocket) throws ClientLaunchException;
}
