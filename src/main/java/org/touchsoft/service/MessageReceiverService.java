package org.touchsoft.service;

import org.touchsoft.model.SocketWrapper;

public interface MessageReceiverService {
    boolean receive(SocketWrapper clientSocketReceiver);

    void stop(SocketWrapper clientSocketReceiver);
}
