package org.touchsoft.model;

import org.touchsoft.exception.AcceptNotifierException;
import org.touchsoft.exception.AccepterInitiationException;
import org.touchsoft.exception.ReceiverClosingException;

import java.io.IOException;
import java.net.ServerSocket;

public class NotifierAccepter {
    private ServerSocket serverSocket;

    public NotifierAccepter(int port) throws AccepterInitiationException {
        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new AccepterInitiationException(e.getMessage());
        }
    }

    public SocketWrapper acceptNotifier() throws AcceptNotifierException {
        try {
            return new SocketWrapper(this.serverSocket.accept());
        } catch (IOException e) {
            throw new AcceptNotifierException(e.getMessage());
        }
    }

    public void close() throws ReceiverClosingException {
        try {
            serverSocket.close();
        } catch (IOException e) {
            throw new ReceiverClosingException(e.getMessage());
        }
    }

    public boolean isClosed() {
        return serverSocket.isClosed();
    }

    public int getLocalPort() {
        return serverSocket.getLocalPort();
    }
}
