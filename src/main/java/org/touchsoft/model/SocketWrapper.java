package org.touchsoft.model;

import org.touchsoft.exception.SocketCreateException;
import org.touchsoft.exception.SocketGetOutputStreamException;
import org.touchsoft.exception.SocketCloseException;
import org.touchsoft.exception.SocketGetInputStreamException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class SocketWrapper {
    private Socket socket;

    public SocketWrapper(Socket socket) throws SocketCreateException {
        this.socket = socket;
    }

    public SocketWrapper(String host, int port) throws SocketCreateException {
        try {
            this.socket = new Socket(host, port);
        } catch (IOException e) {
            throw new SocketCreateException(e.getMessage());
        }
    }

    public InputStream getInputStream() throws SocketGetInputStreamException {
        try {
            return this.socket.getInputStream();
        } catch (IOException e) {
            throw new SocketGetInputStreamException(e.getMessage());
        }
    }

    public OutputStream getOutputStream() throws SocketGetOutputStreamException {
        try {
            return this.socket.getOutputStream();
        } catch (IOException e) {
            throw new SocketGetOutputStreamException(e.getMessage());
        }
    }

    public void closeSocket() throws SocketCloseException {
        try {
            this.socket.close();
        } catch (IOException e) {
            throw new SocketCloseException(e.getMessage());
        }
    }

    public boolean isClosed() {
        return this.socket.isClosed();
    }

    public InetAddress getLocalAddress() {
        return this.socket.getLocalAddress();
    }

    public InetAddress getInetAddress() {
        return this.socket.getInetAddress();
    }
}
