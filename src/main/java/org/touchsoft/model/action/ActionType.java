package org.touchsoft.model.action;

import java.io.Serializable;

public enum ActionType implements Serializable {
    SEND_MESSAGE, LEAVE, REGISTER, FORGET_INTERLOCUTOR, FORGOTTEN;
}
