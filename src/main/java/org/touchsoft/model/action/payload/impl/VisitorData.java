package org.touchsoft.model.action.payload.impl;

import org.touchsoft.model.action.payload.Payload;

import java.util.Objects;

/**
 * Created by Acer on 003 03.11.18.
 */
public class VisitorData implements Payload {
    private static final long serialVersionUID = -3634881558798466031L;

    private String name;
    private Role role;

    public VisitorData(String name, Role role) {
        this.name = name;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "VisitorData{" +
                "name='" + name + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisitorData that = (VisitorData) o;
        return Objects.equals(name, that.name) &&
                role == that.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, role);
    }
}