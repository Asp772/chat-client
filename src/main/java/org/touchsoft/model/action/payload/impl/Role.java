package org.touchsoft.model.action.payload.impl;

import org.touchsoft.model.action.payload.Payload;

public enum Role implements Payload {
    ROLE_AGENT {
        public String toString() {
            return "agent";
        }
    },

    ROLE_CLIENT {
        public String toString() {
            return "client";
        }
    };

    public static Role fromString(String text) {
        for (Role b : Role.values()) {
            if (b.toString().equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
