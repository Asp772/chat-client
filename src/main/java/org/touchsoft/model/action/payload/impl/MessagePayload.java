package org.touchsoft.model.action.payload.impl;

import org.touchsoft.model.action.payload.Payload;

import java.util.Objects;

public class MessagePayload implements Payload {
    private static final long serialVersionUID = -1619207971485101625L;

    private String message;

    public MessagePayload(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessagePayload that = (MessagePayload) o;
        return Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }
}