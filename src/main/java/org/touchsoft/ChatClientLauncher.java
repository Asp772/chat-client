package org.touchsoft;

import org.apache.log4j.Logger;
import org.touchsoft.app.ChatClient;
import org.touchsoft.config.ClientConfig;
import org.touchsoft.exception.ChatClientException;
import org.touchsoft.service.MessageReceiverService;
import org.touchsoft.service.MessageSenderService;
import org.touchsoft.service.MessageTransferService;
import org.touchsoft.service.impl.MessageReceiverServiceImpl;
import org.touchsoft.service.impl.MessageSenderServiceImpl;
import org.touchsoft.service.impl.MessageTransferServiceImpl;

public class ChatClientLauncher {
    private final static Logger logger = Logger.getLogger(ChatClientLauncher.class.getName());

    public static void main(String[] args) {
        try {
            logger.info("trying to start app");

            MessageTransferService messageTransferService = new MessageTransferServiceImpl();
            MessageSenderService messageSenderService = new MessageSenderServiceImpl(messageTransferService);
            MessageReceiverService messageReceiverService = new MessageReceiverServiceImpl(messageTransferService);

            ChatClient chatClient = new ChatClient(ClientConfig.SERVER_HOST, ClientConfig.SERVER_PORT, messageSenderService, messageReceiverService);

            chatClient.launch();
        } catch (ChatClientException e) {
            System.err.println("An error has occurred. App has crashed.");
            logger.error(e.getMessage());
        }
    }
}
