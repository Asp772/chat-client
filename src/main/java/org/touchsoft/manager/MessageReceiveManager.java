package org.touchsoft.manager;

import org.apache.log4j.Logger;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.service.MessageReceiverService;

public class MessageReceiveManager extends Thread {
    private final static Logger logger = Logger.getLogger(MessageReceiveManager.class.getName());

    private SocketWrapper clientSocketReceiver;

    private MessageReceiverService messageReceiverService;
    private boolean threadIsAlive;

    public MessageReceiveManager(MessageReceiverService messageReceiverService, SocketWrapper socketWrapper) {
        this.messageReceiverService = messageReceiverService;
        this.clientSocketReceiver = socketWrapper;

        this.threadIsAlive = true;
    }

    public void run() {
        while (this.threadIsAlive) {
            logger.info("Conversation started");
            this.threadIsAlive = this.messageReceiverService.receive(this.clientSocketReceiver);
        }
    }

    public void stopReceiving() {
        this.threadIsAlive = false;

        this.messageReceiverService.stop(this.clientSocketReceiver);
    }
}
