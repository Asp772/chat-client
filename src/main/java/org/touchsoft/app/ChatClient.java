package org.touchsoft.app;

import org.apache.log4j.Logger;
import org.touchsoft.exception.*;
import org.touchsoft.manager.MessageReceiveManager;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.service.MessageReceiverService;
import org.touchsoft.service.MessageSenderService;

public class ChatClient {
    final static Logger logger = Logger.getLogger(ChatClient.class.getName());

    private MessageSenderService messageSenderService;
    private MessageReceiverService messageReceiverService;

    private SocketWrapper clientSocket;

    public ChatClient(String host, int port, MessageSenderService messageSenderService, MessageReceiverService messageReceiverService) throws ChatClientException {
        try {
            this.clientSocket = new SocketWrapper(host, port);
        } catch (SocketCreateException e) {
            logger.debug(e.getMessage());
            throw new ChatClientException(e.getMessage());
        }
        this.messageSenderService = messageSenderService;
        this.messageReceiverService = messageReceiverService;
    }

    public void launch() throws ChatClientException {
        boolean isActive = true;

        MessageReceiveManager messageReceiveManager = null;

        try {
            if (!this.clientSocket.isClosed()) {

                boolean isRegistered = this.messageSenderService.register(this.clientSocket);
                if (isRegistered) {

                    messageReceiveManager = new MessageReceiveManager( this.messageReceiverService, this.clientSocket);
                    messageReceiveManager.start();

                    while (isActive) {
                        isActive = this.messageSenderService.communicate(this.clientSocket);
                    }
                }
            }
        } catch (ClientLaunchException e) {
            logger.debug(e.getMessage());
            throw new ChatClientException(e.getMessage());
        } catch (ClientRunningException e) {
            logger.debug(e.getMessage());
            throw new ChatClientException(e.getMessage());
        } finally {
            logger.debug("trying to stop message receiver manager");
            if (messageReceiveManager != null) {
                logger.debug(" message receiver manager is not null ");
                messageReceiveManager.stopReceiving();
                logger.debug(" message receiver manager stopped");
            }
        }
    }
}
